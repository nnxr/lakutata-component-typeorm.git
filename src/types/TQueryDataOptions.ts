import { Validator } from '@lakutata/core'

export const TConditionFieldOptionsSchema = Validator.Object({
  $eq: Validator.Any.notRequired().default(undefined),
  $ne: Validator.Any.notRequired().default(undefined),
  $in: Validator.Array(Validator.Any).notRequired().default(undefined),
  $gt: Validator.Any.notRequired().default(undefined),
  $gte: Validator.Any.notRequired().default(undefined),
  $lt: Validator.Any.notRequired().default(undefined),
  $lte: Validator.Any.notRequired().default(undefined),
  $between: Validator.Array(Validator.Any).length(2).notRequired().default(undefined),
  $like: Validator.Any.notRequired().default(undefined)
})

export type TConditionFieldOptions = Partial<{
  $eq: any
  $ne: any
  $in: any[]
  $gt: any
  $gte: any
  $lt: any
  $lte: any
  $between: [any, any]
  $like: string
}>

export const TQueryDataOptionsSchema = Validator.Object({
  where: Validator.Array(Validator.Object(TConditionFieldOptionsSchema)).notRequired().default([]),
  or: Validator.Array(Validator.Object(TConditionFieldOptionsSchema)).notRequired().default([]),
  select: Validator.Array(Validator.String).notRequired().default([]),
  distinct: Validator.Array(Validator.String).notRequired().default([]),
  skip: Validator.Number.notRequired().default(0),
  limit: Validator.Number.notRequired().default(128),
  sort: Validator.Object(Validator.String.oneOf(['ASC', 'DESC'])).notRequired().default(undefined)
})

export type TQueryDataOptions = {
  where?: { [key: string]: TConditionFieldOptions }[]
  or?: { [key: string]: TConditionFieldOptions }[]
  select?: string[]
  distinct?: string[]
  skip?: number
  limit?: number
  sort?: { [key: string]: 'ASC' | 'DESC' }
}
