import { Exception } from '@lakutata/core'

export class UsageRestrictionException extends Exception {
  district: string
  errno: number | string = 'E_USAGE_RESTRICTION'
}
