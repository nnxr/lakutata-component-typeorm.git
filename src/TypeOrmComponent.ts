import { Accept, Component, Configurable, Validator } from '@lakutata/core'
import {
  BaseEntity, Between, Brackets,
  DataSource,
  DataSourceOptions,
  Equal,
  In,
  LessThan,
  LessThanOrEqual, Like,
  MoreThan,
  MoreThanOrEqual,
  Not
} from 'typeorm'
import { EntityManager } from 'typeorm/entity-manager/EntityManager'
import { IsolationLevel } from 'typeorm/driver/types/IsolationLevel'
import { SqljsEntityManager } from 'typeorm/entity-manager/SqljsEntityManager'
import { Migration } from 'typeorm/migration/Migration'
import { QueryRunner } from 'typeorm/query-runner/QueryRunner'
import { MongoEntityManager } from 'typeorm/entity-manager/MongoEntityManager'
import { EntityTarget } from 'typeorm/common/EntityTarget'
import { TreeRepository } from 'typeorm/repository/TreeRepository'
import { Repository } from 'typeorm/repository/Repository'
import { MongoRepository } from 'typeorm/repository/MongoRepository'
import { EntityMetadata } from 'typeorm/metadata/EntityMetadata'
import { ReplicationMode } from 'typeorm/driver/types/ReplicationMode'
import { SelectQueryBuilder } from 'typeorm/query-builder/SelectQueryBuilder'
import { EntitySubscriberInterface } from 'typeorm/subscriber/EntitySubscriberInterface'
import { RelationLoader } from 'typeorm/query-builder/RelationLoader'
import { QueryResultCache } from 'typeorm/cache/QueryResultCache'
import { NamingStrategyInterface } from 'typeorm/naming-strategy/NamingStrategyInterface'
import { MigrationInterface } from 'typeorm/migration/MigrationInterface'
import { Logger } from 'typeorm/logger/Logger'
import { Driver } from 'typeorm/driver/Driver'
import { ObjectLiteral } from 'typeorm/common/ObjectLiteral'
import { IQueryDataResult } from './interfaces/IQueryDataResult'
import { TConditionFieldOptions, TQueryDataOptions, TQueryDataOptionsSchema } from './types/TQueryDataOptions'
import { satisfies } from 'semver'
import { UsageRestrictionException } from './exceptions/UsageRestrictionException'

export class TypeOrmComponent extends Component {
  /**
   * TypeOrm连接参数
   * @protected
   */
  @Configurable()
  public readonly options: DataSourceOptions

  /**
   * 数据库连接
   * @protected
   */
  protected _datasource: DataSource

  protected async initialize (): Promise<void> {
    this._datasource = await (new DataSource(this.options)).initialize()
  }

  public get datasource (): DataSource {
    return this._datasource
  }

  public get isConnected (): boolean {
    return this._datasource.isInitialized
  }

  public get driver (): Driver {
    return this._datasource.driver
  }

  public get entityMetadatas (): EntityMetadata[] {
    return this._datasource.entityMetadatas
  }

  public get logger (): Logger {
    return this._datasource.logger
  }

  public get manager (): EntityManager {
    return this._datasource.manager
  }

  public get migrations (): MigrationInterface[] {
    return this._datasource.migrations
  }

  public get namingStrategy (): NamingStrategyInterface {
    return this._datasource.namingStrategy
  }

  public get queryResultCache (): QueryResultCache | undefined {
    return this._datasource.queryResultCache
  }

  public get relationLoader (): RelationLoader {
    return this._datasource.relationLoader
  }

  public get subscribers (): EntitySubscriberInterface<any>[] {
    return this._datasource.subscribers
  }

  public get mongoManager (): MongoEntityManager {
    return this._datasource.mongoManager

  }

  public get sqljsManager (): SqljsEntityManager {
    return this._datasource.sqljsManager
  }

  public async connect (): Promise<void> {
    this._datasource = await this._datasource.initialize()
  }

  public async close (): Promise<void> {
    await this._datasource.destroy()
  }

  public createEntityManager (queryRunner?: QueryRunner): EntityManager {
    return this._datasource.createEntityManager(queryRunner)
  }

  public createQueryBuilder (queryRunner?: QueryRunner): SelectQueryBuilder<any> {
    return this._datasource.createQueryBuilder(queryRunner)
  }

  public createQueryRunner (mode?: ReplicationMode): QueryRunner {
    return this._datasource.createQueryRunner(mode)
  }

  public async dropDatabase (): Promise<void> {
    await this._datasource.dropDatabase()
  }

  public getManyToManyMetadata (entityTarget: EntityTarget<any>, relationPropertyPath: string): EntityMetadata | undefined {
    return this._datasource.getManyToManyMetadata(entityTarget, relationPropertyPath)
  }

  public getMetadata (target: EntityTarget<any>): EntityMetadata {
    return this._datasource.getMetadata(target)
  }

  public getMongoRepository<Entity extends ObjectLiteral> (target: EntityTarget<Entity>): MongoRepository<Entity> {
    return this._datasource.getMongoRepository(target)
  }

  public getRepository<Entity extends ObjectLiteral> (target: EntityTarget<Entity>): Repository<Entity> {
    return this._datasource.getRepository(target)
  }

  public getTreeRepository<Entity extends ObjectLiteral> (target: EntityTarget<Entity>): TreeRepository<Entity> {
    return this._datasource.getTreeRepository(target)
  }

  public hasMetadata (target: EntityTarget<any>): boolean {
    return this._datasource.hasMetadata(target)
  }

  public async query (query: string, parameters?: any[], queryRunner?: QueryRunner): Promise<any> {
    return await this._datasource.query(query, parameters, queryRunner)
  }

  public async runMigrations (options?: {
    transaction?: 'all' | 'none' | 'each';
  }): Promise<Migration[]> {
    return await this._datasource.runMigrations(options)
  }

  public async showMigrations (): Promise<boolean> {
    return await this._datasource.showMigrations()
  }

  public async synchronize (dropBeforeSync?: boolean): Promise<void> {
    await this._datasource.synchronize(dropBeforeSync)
  }

  public async transaction<T> (runInTransaction: (entityManager: EntityManager) => Promise<T>): Promise<T>;
  public async transaction<T> (isolationLevel: IsolationLevel, runInTransaction: (entityManager: EntityManager) => Promise<T>): Promise<T>;
  public async transaction<T> (a, b?): Promise<T> {
    return await this._datasource.transaction(a, b)
  }

  public async undoLastMigration (options?: {
    transaction?: 'all' | 'none' | 'each';
  }): Promise<void> {
    await this._datasource.undoLastMigration(options)
  }

  /**
   * 统一化查询方法
   * @param options
   * @param entity
   * @param entityManager
   */
  @Accept([TQueryDataOptionsSchema, Validator.Any, Validator.Any], {
    strict: false,
    stripUnknown: false
  })
  public async queryData<Entity extends ObjectLiteral> (options: TQueryDataOptions, entity: EntityTarget<Entity>, entityManager?: EntityManager): Promise<IQueryDataResult<Entity>> {
    if (this._datasource.options.type === 'mongodb' || this._datasource.options.type === 'cockroachdb') throw this.generateException(UsageRestrictionException, 'Query data function only support SQL databases')
    let select: string[] = options.select !== undefined ? options.select : []
    let distinct: string[] = options.distinct !== undefined ? options.distinct : []
    //对select和distinct数组进行去重
    const selectSet: Set<string> = new Set()
    const distinctSet: Set<string> = new Set()
    select.forEach(selectItem => selectSet.add(selectItem))
    distinct.forEach(distinctItem => distinctSet.add(distinctItem))
    select = Array.from(selectSet)
    distinct = Array.from(distinctSet)
    const skip: number = options.skip !== undefined ? options.skip : 0
    const limit: number = options.limit !== undefined ? options.limit : 128
    const sort: { [key: string]: 'ASC' | 'DESC' } = options.sort !== undefined ? options.sort : {}
    const where: { [key: string]: TConditionFieldOptions }[] = options.where !== undefined ? options.where : []
    const or: { [key: string]: TConditionFieldOptions }[] = options.or !== undefined ? options.or : []
    const repository: Repository<Entity> = entityManager ? entityManager.getRepository(entity) : this.getRepository(entity)
    let queryBuilder: SelectQueryBuilder<Entity>
    const metadata = this.getMetadata(entity)
    const columnMap: Map<string, string> = new Map()
    metadata.columns.forEach(column => {
      columnMap.set(`entity_${column.propertyName}`, column.propertyName)
    })
    if (distinct.length > 0) {
      queryBuilder = this.datasource.createQueryBuilder().from(qb => {
        qb = qb.from(entity, 'entity')
        const distinctEntityItems: string[] = distinct.map(item => `entity.${item}`)
        metadata.columns.forEach(column => {
          const columnPropertyName: string = column.propertyName
          const columnName: string = `entity.${columnPropertyName}`
          if (distinctEntityItems.includes(columnName)) {
            qb = qb.addSelect(columnName, columnPropertyName).addGroupBy(columnName)
          } else {
            qb = qb.addSelect(`ANY_VALUE(${columnName})`, columnPropertyName)
          }
        })
        return qb
      }, 'distinctEntity')
      queryBuilder.expressionMap.mainAlias!['metadata'] = metadata
      metadata.columns.forEach(column => {
        queryBuilder.addSelect(column.propertyName, `entity_${column.propertyName}`)
      })
    } else {
      queryBuilder = repository.createQueryBuilder('entity')
    }
    let hasAndWhere: boolean = false
    const appendConditionToQueryBuilderHandler = (fieldConditions: {
      [key: string]: TConditionFieldOptions
    }[], func: 'andWhere' | 'orWhere') => {
      fieldConditions.forEach(fieldConditionObject => {
        const fields: string[] = Object.keys(fieldConditionObject)
        const _func: 'andWhere' | 'orWhere' = (!hasAndWhere && func !== 'andWhere') ? 'andWhere' : func
        hasAndWhere = hasAndWhere ? hasAndWhere : _func === 'andWhere'
        queryBuilder = queryBuilder[_func](new Brackets(qb => {
          fields.forEach(field => {
            const valueWithOperatorObject: TConditionFieldOptions = fieldConditionObject[field]
            const operators: string[] = Object.keys(valueWithOperatorObject)
            const conditions: ObjectLiteral[] = []
            operators.forEach(operator => {
              let val: any = valueWithOperatorObject[operator]
              //判断val是否为Date对象，若为Date对象则需要将val还原为Date对象实例
              if (typeof val === 'string') {
                if (val.endsWith('Z') && !isNaN(Date.parse(val))) {
                  if (/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d+Z$/.test(val)) {
                    val = new Date(val)
                  }
                }
              }
              switch (operator) {
                case '$eq': {
                  const equalObject: { [key: string]: any } = {}
                  equalObject[field] = Equal(val)
                  return conditions.push(equalObject)
                }
                case '$ne': {
                  const notEqualObject: { [key: string]: any } = {}
                  notEqualObject[field] = Not(val)
                  return conditions.push(notEqualObject)
                }
                case '$in': {
                  const inObject: { [key: string]: any } = {}
                  inObject[field] = In(val)
                  return conditions.push(inObject)
                }
                case '$gt': {
                  const greaterThanObject: { [key: string]: any } = {}
                  greaterThanObject[field] = MoreThan(val)
                  return conditions.push(greaterThanObject)
                }
                case '$gte': {
                  const greaterThanOrEqualObject: { [key: string]: any } = {}
                  greaterThanOrEqualObject[field] = MoreThanOrEqual(val)
                  return conditions.push(greaterThanOrEqualObject)
                }
                case '$lt': {
                  const lessThanObject: { [key: string]: any } = {}
                  lessThanObject[field] = LessThan(val)
                  return conditions.push(lessThanObject)
                }
                case '$lte': {
                  const lessThanOrEqualObject: { [key: string]: any } = {}
                  lessThanOrEqualObject[field] = LessThanOrEqual(val)
                  return conditions.push(lessThanOrEqualObject)
                }
                case '$between': {
                  const betweenObject: { [key: string]: any } = {}
                  betweenObject[field] = Between(val[0], val[1])
                  return conditions.push(betweenObject)
                }
                case '$like': {
                  const likeObject: { [key: string]: any } = {}
                  likeObject[field] = Like(val)
                  return conditions.push(likeObject)
                }
              }
            })
            qb.andWhere(conditions)
          })
        }))
      })
    }
    appendConditionToQueryBuilderHandler(where, 'andWhere')
    appendConditionToQueryBuilderHandler(or, 'orWhere')
    let getCountHandler: () => Promise<number> = () => new Promise<number>((resolve, reject) => queryBuilder.clone().getCount().then(resolve).catch(reject))
    let getDataHandler: () => Promise<Entity[]> = () => new Promise<Entity[]>((resolve, reject) => {
      try {
        return queryBuilder.execute().then(results => {
          return resolve(results.map(result => {
            const convertedResult: { [key: string]: any } = {}
            Object.keys(result).forEach(key => {
              const name: string | undefined = columnMap.get(key)
              if (name) {
                if (select.length > 0 && !select.includes(name)) return
                convertedResult[name] = result[key]
              } else {
                convertedResult[key] = result[key]
              }
            })
            return convertedResult
          }))
        }).catch(reject)
      } catch (e) {
        return reject(e)
      }
    })
    queryBuilder = queryBuilder.orderBy(sort).offset(skip).limit(limit)
    const [total, data] = await Promise.all([getCountHandler(), getDataHandler()])
    return {
      data: data,
      count: data.length,
      skip: skip,
      limit: limit,
      total: total
    }
  }
}

/**
 * 统一化查询方法接口及类型暴露
 */
export { IQueryDataResult } from './interfaces/IQueryDataResult'
export { TConditionFieldOptions, TQueryDataOptions, TQueryDataOptionsSchema } from './types/TQueryDataOptions'

/**
 * 暴露错误类
 */
export { UsageRestrictionException } from './exceptions/UsageRestrictionException'

/**
 * Columns
 */
export {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  ObjectIdColumn,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
  ViewColumn
} from 'typeorm'
/**
 * Entities
 */
export {
  ChildEntity,
  Entity,
  TableInheritance
} from 'typeorm'
/**
 * EntityView
 */
export { ViewEntity } from 'typeorm'
/**
 * Listeners
 */
export {
  AfterInsert,
  AfterLoad,
  AfterRecover,
  AfterRemove,
  AfterSoftRemove,
  AfterUpdate,
  BeforeInsert,
  BeforeRecover,
  BeforeRemove,
  BeforeSoftRemove,
  BeforeUpdate,
  EventSubscriber
} from 'typeorm'
/**
 * Options
 */
export {
  ColumnOptions,
  AddUserOptions,
  ChangeStreamOptions,
  CollectionOptions,
  DataSourceOptions,
  DbOptions,
  EntityOptions,
  EntitySchemaColumnOptions,
  EntitySchemaEmbeddedColumnOptions,
  EntitySchemaIndexOptions,
  EntitySchemaOptions,
  EntitySchemaRelationOptions,
  FileLoggerOptions,
  FindManyOptions,
  FindOneAndDeleteOptions,
  FindOneOptions,
  FindTreeOptions,
  GridFSBucketOptions,
  GridFSBucketReadStreamOptions,
  GridFSBucketWriteStreamOptions,
  IndexInformationOptions,
  IndexOptions,
  JoinColumnOptions,
  JoinTableOptions,
  ListCollectionsOptions,
  ListIndexesOptions,
  LoggerOptions,
  MongoClientOptions,
  PrimaryColumnOptions,
  RelationOptions,
  RemoveOptions,
  SaveOptions,
  TableCheckOptions,
  TableColumnOptions,
  TableExclusionOptions,
  TableForeignKeyOptions,
  TableIndexOptions,
  TableOptions,
  TableUniqueOptions,
  TransactionOptions,
  ViewOptions,
  ConnectionOptionsReader,
  FindOptionsOrder,
  FindOptionsOrderProperty,
  FindOptionsOrderValue,
  FindOptionsRelations,
  FindOptionsRelationsProperty,
  FindOptionsSelect,
  FindOptionsSelectProperty,
  FindOptionsUtils,
  FindOptionsWhere,
  FindOptionsWhereProperty,
  AggregateOptions,
  OperationOptions,
  CloseOptions,
  AutoEncryptionOptions,
  AggregationCursorOptions,
  CollationOptions,
  BSONSerializeOptions,
  BulkWriteOptions,
  CountOptions,
  CommandOperationOptions,
  ConnectOptions,
  AutoEncryptionTlsOptions,
  ClientMetadataOptions,
  DeleteOptions,
  ClientSessionOptions,
  ClusteredCollectionOptions,
  CollStatsOptions,
  DistinctOptions,
  EvalOptions,
  ConnectionPoolOptions,
  CountDocumentsOptions,
  CreateCollectionOptions,
  CreateIndexesOptions,
  CursorStreamOptions,
  DbStatsOptions,
  FindOptions,
  HedgeOptions,
  DropCollectionOptions,
  ExplainOptions,
  InsertOneOptions,
  DropDatabaseOptions,
  DropIndexesOptions,
  EndSessionOptions,
  MongoOptions,
  EstimatedDocumentCountOptions,
  FindOneAndReplaceOptions,
  MonitorOptions,
  FindOneAndUpdateOptions,
  ProxyOptions,
  AbstractCursorOptions,
  RenameOptions,
  ListDatabasesOptions,
  MongoCredentialsOptions,
  MongoNetworkErrorOptions,
  ReplaceOptions,
  ReadPreferenceLikeOptions,
  ReadPreferenceOptions,
  RemoveUserOptions,
  RunCommandOptions,
  UpdateOptions,
  SelectServerOptions,
  SetProfilingLevelOptions,
  StreamDescriptionOptions,
  PrepareLogMessagesOptions,
  ProfilingLevelOptions,
  ReadPreferenceFromOptions,
  SupportedTLSConnectionOptions,
  SupportedNodeConnectionOptions,
  SupportedSocketOptions,
  SupportedTLSSocketOptions,
  TimeSeriesCollectionOptions,
  TopologyDescriptionOptions,
  ValidateCollectionOptions,
  WriteConcernOptions,
  GridFSBucketReadStreamOptionsWithRevision
} from 'typeorm'
/**
 * Relations
 */
export { JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, RelationId } from 'typeorm'
/**
 * Tree
 */
export { Tree, TreeChildren, TreeLevelColumn, TreeParent } from 'typeorm'
/**
 * Errors
 */
export {
  AlreadyHasActiveConnectionError,
  CannotAttachTreeChildrenEntityError,
  CannotConnectAlreadyConnectedError,
  CannotCreateEntityIdMapError,
  CannotDetermineEntityError,
  CannotExecuteNotConnectedError,
  CannotGetEntityManagerNotConnectedError,
  CannotReflectMethodParameterTypeError,
  CircularRelationsError,
  ColumnTypeUndefinedError,
  ConnectionIsNotSetError,
  ConnectionNotFoundError,
  CustomRepositoryCannotInheritRepositoryError,
  CustomRepositoryDoesNotHaveEntityError,
  CustomRepositoryNotFoundError,
  DataTypeNotSupportedError,
  DriverOptionNotSetError,
  DriverPackageNotInstalledError,
  EntityMetadataNotFoundError,
  EntityNotFoundError,
  EntityPropertyNotFoundError,
  FindRelationsNotFoundError,
  InitializedRelationError,
  InsertValuesMissingError,
  LimitOnUpdateNotSupportedError,
  LockNotSupportedOnGivenDriverError,
  MetadataAlreadyExistsError,
  MetadataWithSuchNameAlreadyExistsError,
  MissingDeleteDateColumnError,
  MissingDriverError,
  MissingJoinColumnError,
  MissingJoinTableError,
  MissingPrimaryColumnError,
  MustBeEntityError,
  NamingStrategyNotFoundError,
  WriteConcernError,
  NoConnectionForRepositoryError,
  NoConnectionOptionError,
  NoNeedToReleaseEntityManagerError,
  NoVersionOrUpdateDateColumnError,
  OffsetWithoutLimitNotSupportedError,
  OptimisticLockCanNotBeUsedError,
  OptimisticLockVersionMismatchError,
  PersistedEntityNotFoundError,
  PessimisticLockTransactionRequiredError,
  PrimaryColumnCannotBeNullableError,
  QueryFailedError,
  QueryRunnerAlreadyReleasedError,
  QueryRunnerProviderAlreadyReleasedError,
  RepositoryNotTreeError,
  ReturningStatementNotSupportedError,
  SubjectRemovedAndUpdatedError,
  SubjectWithoutIdentifierError,
  TransactionAlreadyStartedError,
  TransactionNotStartedError,
  TreeRepositoryNotSupportedError,
  TypeORMError,
  UpdateValuesMissingError,
  UsingJoinColumnIsNotAllowedError,
  UsingJoinColumnOnlyOnOneSideAllowedError,
  UsingJoinTableIsNotAllowedError,
  UsingJoinTableOnlyOnOneSideAllowedError,
  WriteError,
  MongoError
} from 'typeorm'
/**
 * EntityManager
 */
export { EntityManager, MongoEntityManager } from 'typeorm'
export { EntitySchema, ValueTransformer } from 'typeorm'
export { Check, Exclusion, Generated, Unique, Index } from 'typeorm'
export { BaseEntity, MongoRepository, Repository, TreeRepository } from 'typeorm'
export { Migration, MigrationExecutor, MigrationInterface } from 'typeorm'

export * from 'typeorm'
