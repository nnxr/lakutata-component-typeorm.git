import { createApp } from '@lakutata/core'
import { TypeOrmComponent } from '../TypeOrmComponent'
import path from 'path'
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions'
import { TestEntity } from './entities/TestEntity'

(async () => {
  try {
    const app = await createApp({
      id: 'test.app',
      name: 'tester',
      plugins: [],
      modules: {},
      components: {
        test: {
          class: TypeOrmComponent,
          options: {
            name: 'test',
            type: 'mysql',
            host: '192.168.0.145',
            port: 3306,
            username: 'root',
            password: '20160329',
            database: 'test',
            synchronize: true,
            entities: [
              `${path.resolve(__dirname, './entities/*')}`
            ]
          } as MysqlConnectionOptions
        }
      }
    })
    const testComponent: TypeOrmComponent = app.Components.get<TypeOrmComponent>('test')
    // setInterval(async () => {
    console.log('start')
    const result = await testComponent.queryData(JSON.parse(JSON.stringify({
      // where: [{ name: { $like: '%1%' } }],
      or: [
        // { createdAt: { $lte: new Date('2029-11-07 12:33:00') } },
        {
        createdAt: { $lte: new Date('2029-11-07 12:33:00') },
        sort: { $gt: 1 },
        // name: { $eq: 'abc' }
        // content: { $like: '%u%' }
      }],
      // select: ['id', 'name', 'sort', 'content'],
      distinct: ['name'],
      // select: ['id','sort'],
      limit: 300,
      skip: 0,
      sort: { sort: 'DESC' }
    })), TestEntity)

    console.log('result', result)
    // }, 1000)

    // const repo = testComponent.getRepository(TestEntity)
    // const te = new TestEntity()
    // te.name = `${Date.now()}`
    // te.content = RandomString(6)
    // te.sort = ((await testComponent.getRepository(TestEntity).createQueryBuilder().select('Max(sort)', 'sort').getOne())?.sort ?? 0) + 1
    // await repo.save(te)
    // console.log(testComponent.isConnected)
  } catch (e) {
    console.error(e)
  }
})()

// 生成随机整数 具有范围约束[min, max)
// min 最小值
// max 最大值
function RangeInteger (min: number, max: number) {
  const range = max - min
  const value = Math.floor(Math.random() * range) + min
  return value
}

// 生成指定长度的随机 含[a~z]的字符串,
// length 指定字符串长度
// toUpper 首字母是否大写
function RandomString (length: number, firstToUpper?: boolean) {
  let str = ''
  for (let i = 0; i < length; i++) {
    if (firstToUpper && i == 1) {
      str += String.fromCharCode(RangeInteger(97, 123)).toUpperCase() // 首字母大写
      continue
    }
    str += String.fromCharCode(RangeInteger(97, 123))
  }

  return str
}


