export interface IQueryDataResult<DataType = any> {
  data: DataType[]
  count: number
  skip: number
  limit: number
  total: number
}
