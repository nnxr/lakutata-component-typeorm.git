<div style="text-align:center">
	<img src="https://gitee.com/nnxr/lakutata-component-typeorm/raw/master/assets/lakutata-banner.png" />
</div>

[![NPM Version](https://img.shields.io/npm/v/@lakutata-component/typeorm?color=informational&style=flat-square)](https://npmjs.org/package/@lakutata-component/typeorm)
[![NODE Version](https://img.shields.io/node/v/@lakutata-component/typeorm?color=informational&style=flat-square)](https://npmjs.org/package/@lakutata-component/typeorm)
[![Known Vulnerabilities](https://snyk.io/test/npm/@lakutata-component/typeorm/badge.svg?style=flat-square)](https://snyk.io/test/npm/@lakutata-component/typeorm)
[![NPM Download](https://img.shields.io/npm/dm/@lakutata-component/typeorm?style=flat-square)](https://npmjs.org/package/@lakutata-component/typeorm)

## Quickstart

#### Step 1: Configure typeorm component

    const app = await createApp({
            id: 'test.app',
            name: 'tester',
            components: {
                typeorm: {
                    class: TypeOrmComponent,
                    options: {
                        type: 'mysql',
                        host: 'localhost',
                        port: 3306,
                        username: 'user',
                        password: 'passwd',
                        database: 'database',
                        synchronize: true,
                        entities: [
                            `${path.resolve(__dirname, './entities/*')}`
                        ]
                    } as MysqlConnectionOptions
                }
            }
        })

#### Step 2: Get typeorm component

    const typeorm: TypeOrmComponent = app.Components.get<TypeOrmComponent>('typeorm')

#### Step 3: Use it

    typeorm.getRepository(...)
    typeorm.getMongoRepository(...)
    typeorm.transaction(...)
    typeorm.query(...)
    ...

> @lakutata/core required.

## Documentations

See [Typeorm official documents](https://github.com/typeorm/typeorm/tree/master/docs)

## How to Contribute

Please let us know how can we help. Do check out [issues](https://gitee.com/nnxr/lakutata-component-typeorm/issues) for bug reports or suggestions first.

## License

[MIT](LICENSE)